import { LinkedList } from '@/linkedList';

describe('LinkedList Testing', () => {
  let values: number[] = [];
  let list = new LinkedList<number>();

  beforeEach(() => {
    values = [1, 2, 3, 4];
    list = new LinkedList<number>(values);
  });

  test('Check constructor', () => {
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    list = new LinkedList([1, 2, 3, 4], false);
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    list = new LinkedList<number>();
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });

  test('Check push and pop items', () => {
    list = new LinkedList<number>();

    list.pushBack(1);
    list.pushFront(2);
    list.pushBack(3);
    list.pushFront(4);

    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(3);
    expect(list.popFront()).toBe(4);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    list.pushBack(5);
    list.pushFront(6);
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(5);
    expect(list.popFront()).toBe(6);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(1);
    expect(list.popFront()).toBe(2);
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();

    list.pushBack(7);
    list.pushFront(8);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(7);
    expect(list.popFront()).toBe(8);
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });

  test('Check forEach', () => {
    // Forward forEach direction
    let i = 0;
    list.forEach(({ data }) => {
      expect(data).toBe(values[i++]);
    });
    expect(i).toBe(values.length);

    // Backward forEach direction
    i = values.length;
    list.forEach(({ data }) => {
      expect(data).toBe(values[--i]);
    }, false);
    expect(i).toBe(0);

    // Stop forEach
    i = 0;
    list.forEach(({ data }, stop) => {
      expect(data).toBe(values[i++]);
      if (i === 2) stop();
    });
    expect(i).toBe(2);
  });

  test('Check iterator', () => {
    const arr = [...list];
    arr.map((item) => item.data).forEach((item, ind) => {
      expect(item).toBe(values[ind]);
    });
  });

  test('Check removing', () => {
    list.forEach((item) => {
      if (item.data % 2) list.remove(item);
    });
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    const sum = [...list].reduce((acc, cur) => acc + cur.data, 0);
    expect(sum).toBe(6);
  });

  test('Check clear', () => {
    list.clear();
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });
});

import { EventedLinkedList, LinkedListEvent } from '@/linkedList';

describe('EventedLinkedList Testing', () => {
  let values: number[] = [];
  let list = new EventedLinkedList<number>();

  beforeEach(() => {
    values = [1, 2, 3, 4];
    list = new EventedLinkedList<number>(values);
  });

  test('Check constructor', () => {
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    list = new EventedLinkedList<number>([1, 2, 3, 4], false);
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    list = new EventedLinkedList<number>();
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });

  test('Check push and pop items', () => {
    list = new EventedLinkedList<number>();

    list.pushBack(1);
    list.pushFront(2);
    list.pushBack(3);
    list.pushFront(4);

    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(3);
    expect(list.popFront()).toBe(4);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    list.pushBack(5);
    list.pushFront(6);
    expect(list.getSize()).toBe(4);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(5);
    expect(list.popFront()).toBe(6);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(1);
    expect(list.popFront()).toBe(2);
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();

    list.pushBack(7);
    list.pushFront(8);
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    expect(list.popBack()).toBe(7);
    expect(list.popFront()).toBe(8);
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });

  test('Check forEach', () => {
    // Forward forEach direction
    let i = 0;
    list.forEach(({ data }) => {
      expect(data).toBe(values[i++]);
    });
    expect(i).toBe(values.length);

    // Backward forEach direction
    i = values.length;
    list.forEach(({ data }) => {
      expect(data).toBe(values[--i]);
    }, false);
    expect(i).toBe(0);

    // Stop forEach
    i = 0;
    list.forEach(({ data }, stop) => {
      expect(data).toBe(values[i++]);
      if (i === 2) stop();
    });
    expect(i).toBe(2);
  });

  test('Check iterator', () => {
    const arr = [...list];
    arr.map((item) => item.data).forEach((item, ind) => {
      expect(item).toBe(values[ind]);
    });
  });

  test('Check removing', () => {
    list.forEach((item) => {
      if (item.data % 2) list.remove(item);
    });
    expect(list.getSize()).toBe(2);
    expect(list.isEmpty()).toBeFalsy();

    const sum = [...list].reduce((acc, cur) => acc + cur.data, 0);
    expect(sum).toBe(6);
  });

  test('Check clear', () => {
    list.clear();
    expect(list.getSize()).toBe(0);
    expect(list.isEmpty()).toBeTruthy();
  });

  test('Check event handling', () => {
    const counters = {
      [LinkedListEvent.HeadChanged]: 0,
      [LinkedListEvent.TailChanged]: 0,
      [`${LinkedListEvent.EmptyChanged}:toEmpty`]: 0,
      [`${LinkedListEvent.EmptyChanged}:toNotEmpty`]: 0,
    };

    list.on(LinkedListEvent.HeadChanged, () => {
      counters[LinkedListEvent.HeadChanged]++;
    }).on(LinkedListEvent.TailChanged, () => {
      counters[LinkedListEvent.TailChanged]++;
    }).on(LinkedListEvent.EmptyChanged, (isEmpty) => {
      const value = isEmpty ? 'toEmpty' : 'toNotEmpty';
      counters[`${LinkedListEvent.EmptyChanged}:${value}`]++;
    });

    list.popFront();
    list.popBack();
    list.popFront();
    list.popBack();
    list.clear();

    list.pushFront(5);
    list.pushBack(6);
    list.pushFront(7);
    list.clear();

    expect(counters[LinkedListEvent.HeadChanged]).toBe(6);
    expect(counters[LinkedListEvent.TailChanged]).toBe(5);
    expect(counters[`${LinkedListEvent.EmptyChanged}:toEmpty`]).toBe(2);
    expect(counters[`${LinkedListEvent.EmptyChanged}:toNotEmpty`]).toBe(1);
  });
});

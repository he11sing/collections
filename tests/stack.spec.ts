import Stack from '@/stack';

describe('Stack Testing', () => {
  let stack = new Stack();

  beforeEach(() => {
    stack = new Stack<number>();
  });

  test('Check constructor', () => {
    expect(stack.getSize()).toBe(0);
    expect(stack.isEmpty()).toBeTruthy();

    stack = new Stack([1, 2, 3, 4]);
    expect(stack.getSize()).toBe(4);
    expect(stack.isEmpty()).toBeFalsy();
  });

  test('Check push and pop items', () => {
    stack.push(3);
    stack.push(5);

    expect(stack.getSize()).toBe(2);
    expect(stack.isEmpty()).toBeFalsy();

    expect(stack.pop()).toBe(5);
    expect(stack.getSize()).toBe(1);
    expect(stack.isEmpty()).toBeFalsy();

    stack.push(7);
    expect(stack.getSize()).toBe(2);
    expect(stack.isEmpty()).toBeFalsy();

    expect(stack.pop()).toBe(7);
    expect(stack.getSize()).toBe(1);
    expect(stack.isEmpty()).toBeFalsy();

    expect(stack.pop()).toBe(3);
    expect(stack.getSize()).toBe(0);
    expect(stack.isEmpty()).toBeTruthy();

    stack.push(9);
    expect(stack.getSize()).toBe(1);
    expect(stack.isEmpty()).toBeFalsy();

    expect(stack.pop()).toBe(9);
    expect(stack.getSize()).toBe(0);
    expect(stack.isEmpty()).toBeTruthy();
  });

  test('Check clear', () => {
    stack.push(3);
    stack.push(5);
    stack.clear();

    expect(stack.getSize()).toBe(0);
    expect(stack.isEmpty()).toBeTruthy();
  });
});

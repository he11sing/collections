import Queue from '@/queue';

describe('Queue Testing', () => {
  let queue = new Queue();

  beforeEach(() => {
    queue = new Queue<number>();
  });

  test('Check constructor', () => {
    expect(queue.getSize()).toBe(0);
    expect(queue.isEmpty()).toBeTruthy();

    queue = new Queue([1, 2, 3, 4]);
    expect(queue.getSize()).toBe(4);
    expect(queue.isEmpty()).toBeFalsy();
  });

  test('Check push and pop items', () => {
    queue.push(3);
    queue.push(5);

    expect(queue.getSize()).toBe(2);
    expect(queue.isEmpty()).toBeFalsy();

    expect(queue.pop()).toBe(3);
    expect(queue.getSize()).toBe(1);
    expect(queue.isEmpty()).toBeFalsy();

    queue.push(7);
    expect(queue.getSize()).toBe(2);
    expect(queue.isEmpty()).toBeFalsy();

    expect(queue.pop()).toBe(5);
    expect(queue.getSize()).toBe(1);
    expect(queue.isEmpty()).toBeFalsy();

    expect(queue.pop()).toBe(7);
    expect(queue.getSize()).toBe(0);
    expect(queue.isEmpty()).toBeTruthy();

    queue.push(9);
    expect(queue.getSize()).toBe(1);
    expect(queue.isEmpty()).toBeFalsy();

    expect(queue.pop()).toBe(9);
    expect(queue.getSize()).toBe(0);
    expect(queue.isEmpty()).toBeTruthy();
  });

  test('Check clear', () => {
    queue.push(3);
    queue.push(5);
    queue.clear();

    expect(queue.getSize()).toBe(0);
    expect(queue.isEmpty()).toBeTruthy();
  });
});

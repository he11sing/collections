export class StackItem<T> {
  data: T;
  prev: StackItem<T>|null;

  constructor(data: T, prev: StackItem<T>|null = null) {
    this.data = data;
    this.prev = prev;
  }
}

export default class Stack<T> {
  private size: number = 0;
  private tail: StackItem<T>|null = null;

  constructor(items: T[] = []) {
    const { length } = items;
    for (let i = 0; i < length; ++i) {
      this.push(items[i]);
    }
  }

  public push(data: T): void {
    this.size++;
    this.tail = new StackItem<T>(data, this.tail);
  }

  public pop(): T|undefined {
    if (!this.tail) return undefined;

    const { data } = this.tail;
    this.tail = this.tail.prev;
    this.size--;
    if (!this.size) {
      this.tail = null;
    }
    return data;
  }

  public getSize(): number {
    return this.size;
  }

  public isEmpty(): boolean {
    return !this.size;
  }

  public clear(): void {
    this.size = 0;
    this.tail = null;
  }
}

export class QueueItem<T> {
  data: T;
  next: QueueItem<T>|null;

  constructor(data: T, next: QueueItem<T>|null = null) {
    this.data = data;
    this.next = next;
  }
}

export default class Queue<T> {
  private size: number = 0;
  private head: QueueItem<T>|null = null;
  private tail: QueueItem<T>|null = null;

  constructor(items: T[] = []) {
    const { length } = items;
    for (let i = 0; i < length; ++i) {
      this.push(items[i]);
    }
  }

  public push(data: T): void {
    const item = new QueueItem<T>(data);
    if (this.tail) {
      this.tail.next = item;
    } else {
      this.head = item;
    }
    this.tail = item;
    this.size++;
  }

  public pop(): T|undefined {
    if (!this.head) return undefined;

    const { data } = this.head;
    this.head = this.head.next;
    this.size--;

    if (!this.size) {
      this.tail = null;
    }

    return data;
  }

  public getSize(): number {
    return this.size;
  }

  public isEmpty(): boolean {
    return !this.size;
  }

  public clear(): void {
    this.size = 0;
    this.head = null;
    this.tail = null;
  }
}

import { ItemPtr, LinkedListItem } from './types';

export default class LinkedList<T> {
  private size: number = 0;
  private head: ItemPtr<T> = null;
  private tail: ItemPtr<T> = null;

  constructor(items: T[] = [], isForward: boolean = true) {
    const { length } = items;
    const push = isForward ? this.pushBack.bind(this) : this.pushFront.bind(this);

    for (let i = 0; i < length; ++i) {
      push(items[i]);
    }
  }

  public pushFront(data: T): LinkedListItem<T> {
    const item = new LinkedListItem(data, null, this.head);
    if (this.head) {
      this.head.prev = item;
    }

    if (!this.tail) this.tail = item;
    this.head = item;
    this.size++;
    return item;
  }

  public pushBack(data: T): LinkedListItem<T> {
    const item = new LinkedListItem(data, this.tail);
    if (this.tail) {
      this.tail.next = item;
    }

    if (!this.head) this.head = item;
    this.tail = item;
    this.size++;
    return item;
  }

  public popFront(): T|null {
    if (!this.head) return null;
    return this.remove(this.head);
  }

  public popBack(): T|null {
    if (!this.tail) return null;
    return this.remove(this.tail);
  }

  public remove(item: LinkedListItem<T>): T {
    if (item === this.head) {
      this.head = item.next;
    }
    if (item === this.tail) {
      this.tail = item.prev;
    }

    /* eslint no-param-reassign: off */
    if (item.prev) item.prev.next = item.next;
    if (item.next) item.next.prev = item.prev;
    this.size--;
    return item.data;
  }

  public getSize(): number {
    return this.size;
  }

  public isEmpty(): boolean {
    return !this.size;
  }

  public clear(): void {
    this.size = 0;
    this.head = null;
    this.tail = null;
  }

  public forEach(callback: (item: LinkedListItem<T>, stop: () => void) => void, isForward: boolean = true): this {
    let curItem = isForward ? this.head : this.tail;
    let stopFlag = false;
    const stop = () => { stopFlag = true; };

    while (curItem && !stopFlag) {
      callback(curItem, stop);
      curItem = isForward ? curItem.next : curItem.prev;
    }
    return this;
  }

  public [Symbol.iterator]() {
    let res;
    let curItem = this.head;

    return {
      next: () => {
        res = { value: <LinkedListItem<T>> curItem, done: !curItem };
        if (curItem) curItem = curItem.next;
        return res;
      },
    };
  }
}

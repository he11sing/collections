export type ItemPtr<T> = LinkedListItem<T>|null;

export class LinkedListItem<T> {
  data: T;
  prev: ItemPtr<T>;
  next: ItemPtr<T>;

  constructor(data: T, prev: ItemPtr<T> = null, next: ItemPtr<T> = null) {
    this.data = data;
    this.prev = prev;
    this.next = next;
  }
}

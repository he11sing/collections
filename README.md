## @he11sing/collections
Collection of main data structures based on Typescript

### Contents
1. [ LinkedList ](#linkedList)
2. [ EventedLinkedList ](#eventedLinkedList)
3. [ Stack ](#stack)
4. [ Queue ](#queue)

<a name="linkedList"></a>
### LinkedList
This is doubly linked list realization.
```typescript
import { LinkedList } from '@he11sing/collections';

const list = new LinkedList<number>(); // May create list from array (by forward or backward direction)
console.log(list.isEmpty()); // true

list.pushBack(3);
list.pushFront(2);
list.pushBack(4);
list.pushFront(1);
console.log(list.isEmpty()); // false
console.log(list.getSize()); // 4

// Traversing list items from head to tail direction
list.forEach(({ data }, stop) => { // Use stop function to break forEach loop
  console.log(data); // 1 2 3 4
});

// Traversing list items from tail to head direction
list.forEach((item, stop) => { // Use stop function to break forEach loop
  if (item.data % 2) list.remove(item); // remove odd items from list
}, false);

// Traversing list items by iterator
for (const item of list) {
  console.log(item.data); // 2 4
}

// Convert list to array
const sum = [...list].reduce((acc, cur) => acc + cur.data, 0);
console.log(sum); // 6

list.clear(); // Remove all list items
```
<a name="eventedLinkedList"></a>
### EventedLinkedList
Provides the same API as the LinkedList, but allows you to handle some events
```typescript
import { EventedLinkedList, LinkedListEvent } from '@he11sing/collections';

const list = new EventedLinkedList<number>(); // May create list from array (by forward or backward direction)
console.log(list.isEmpty()); // true

list.on(LinkedListEvent.HeadChanged, () => {
  console.log(`${LinkedListEvent.HeadChanged} emitted`);
}).on(LinkedListEvent.TailChanged, () => {
  console.log(`${LinkedListEvent.TailChanged} emitted`);
}).on(LinkedListEvent.EmptyChanged, (isEmpty) => {
  console.log(`${LinkedListEvent.EmptyChanged} emitted: ${isEmpty}`);
});
```

<a name="stack"></a>
### Stack
This is stack data structure realization based on linked list.
```typescript
import { Stack } from '@he11sing/collections';

const stack = new Stack<string>(); // May create stack from array
console.log(stack.isEmpty()); // true

stack.push('One');
stack.push('Two');
stack.push('Three');
console.log(stack.isEmpty()); // false
console.log(stack.getSize()); // 3

console.log(stack.pop()); // Three
stack.push('Four');

while(!stack.isEmpty()) {
  console.log(stack.pop()); // Four Two One
}

stack.clear(); // Remove all stack items
```

<a name="queue"></a>
### Queue
This is queue data structure realization based on linked list.
```typescript
import { Queue } from '@he11sing/collections';

const queue = new Queue<string>(); // May create queue from array
console.log(queue.isEmpty()); // true

queue.push('One');
queue.push('Two');
queue.push('Three');
console.log(queue.isEmpty()); // false
console.log(queue.getSize()); // 3

console.log(queue.pop()); // One
queue.push('Four');

while(!queue.isEmpty()) {
  console.log(queue.pop()); // Two Three Four
}

queue.clear(); // Remove all queue items
```